# Copyright (C) 2017 Michał Góral
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess
import functools
import io

import pytest

from commenter import main

@pytest.fixture
def args(monkeypatch):
    def _f(a):
        try:
            args = a.split()  # accept string
        except AttributeError:
            args = a          # accept list
        monkeypatch.setattr('sys.argv', args)
    return _f


@pytest.fixture
def stdin(monkeypatch):
    def _f(text):
        buf = io.StringIO(text)
        buf.buffer = io.BytesIO(bytes(text, encoding='utf-8'))
        monkeypatch.setattr('sys.stdin', buf)
    return _f


@pytest.fixture(params=['stdin', 'file'])
def call(request, capsys, tmpdir, args, stdin):
    f = tmpdir.mkdir("fileinput_test").join("input.txt")

    def _f(inp, *defines, **opts):
        cmd = ['commenter']

        for define in defines:
            cmd.extend(['-D', define])

        for opt_name, val in opts.items():
            cmd.append(opt_name)
            if val:
                cmd.append(val)

        if request.param == 'stdin':
            stdin(inp)
        elif request.param == 'file':
            f.write(inp)
            cmd.extend(['-i', str(f)])

        args(cmd)
        main()
        out, _ = capsys.readouterr()
        return out
    return _f


def test_passthrough(call):
    inp = 'zażółć gęślą jaźń'
    assert inp == call(inp)
    assert inp == call(inp, 'zażółć')


@pytest.mark.parametrize('prefix', ['# ', '// ', '" ',
                                    '#' , '//' , '"'])
def test_single(call, prefix):
    inp = '%(prefix)scomm:begin foo\n' \
          'foo\n' \
          '%(prefix)scomm:end\n' % dict(prefix=prefix)

    exp = '%(prefix)scomm:begin foo\n' \
          '%(prefix)sfoo\n' \
          '%(prefix)scomm:end\n' % dict(prefix=prefix)

    assert exp == call(inp)
    assert inp == call(exp, 'foo')
    assert inp == call(inp, 'foo')


def test_prefix_whitespaces(call):
    inp = '# comm:begin foo\n' \
          '#foo\n' \
          '# comm:end\n'

    exp = '# comm:begin foo\n' \
          '# #foo\n' \
          '# comm:end\n'

    assert inp == call(inp, 'foo')
    assert exp == call(inp)
    assert exp == call(inp, 'bar')

    assert inp == call(exp, 'foo')

    inp = '#comm:begin foo\n' \
          '#foo\n' \
          '#comm:end\n'

    exp = '#comm:begin foo\n' \
          'foo\n' \
          '#comm:end\n'

    assert exp == call(inp, 'foo')
    assert inp == call(exp)


def test_nested(call):
    inp = '# comm:begin foo\n' \
          '# foo\n' \
          '# comm:begin bar\n' \
          '# bar\n' \
          '# comm:end\n' \
          '# comm:end\n' \

    exp_foo = '# comm:begin foo\n' \
              'foo\n' \
              '# comm:begin bar\n' \
              '# bar\n' \
              '# comm:end\n' \
              '# comm:end\n' \

    exp_foo_bar = '# comm:begin foo\n' \
                  'foo\n' \
                  '# comm:begin bar\n' \
                  'bar\n' \
                  '# comm:end\n' \
                  '# comm:end\n' \

    assert exp_foo == call(inp, 'foo')
    assert exp_foo_bar == call(inp, 'foo', 'bar')
    assert inp == call(inp, 'bar', 'bar')


def test_bool_not(call):
    inp = '# comm:begin !foo\n' \
          '# foo bar\n' \
          '# comm:end\n' \

    exp = '# comm:begin !foo\n' \
          'foo bar\n' \
          '# comm:end\n' \

    assert exp == call(inp)
    assert exp == call(inp, 'bar')
    assert exp == call(inp, 'bar', 'baz')

    assert inp == call(exp, 'foo')
    assert inp == call(inp, 'foo')
    assert inp == call(inp, 'foo', 'bar')


def test_bool_and(call):
    inp = '# comm:begin foo && bar\n' \
          '# foo bar\n' \
          '# comm:end\n' \

    exp = '# comm:begin foo && bar\n' \
          'foo bar\n' \
          '# comm:end\n' \

    assert exp == call(inp, 'foo', 'bar')
    assert inp == call(inp, 'foo')
    assert inp == call(inp, 'bar')

    assert inp == call(exp)
    assert inp == call(exp, 'foo')
    assert inp == call(exp, 'bar')

def test_bool_or(call):
    inp = '# comm:begin foo || bar\n' \
          '# foo bar\n' \
          '# comm:end\n' \

    exp = '# comm:begin foo || bar\n' \
          'foo bar\n' \
          '# comm:end\n' \

    assert exp == call(inp, 'foo', 'bar')
    assert exp == call(inp, 'foo')
    assert exp == call(inp, 'bar')
    assert inp == call(inp, 'baz')

    assert inp == call(exp)
    assert inp == call(exp, 'baz')


def test_bool_left_to_right(call):
    inp = '# comm:begin foo && bar && baz\n' \
          '# foo bar baz\n' \
          '# comm:end\n' \

    exp = '# comm:begin foo && bar && baz\n' \
          'foo bar baz\n' \
          '# comm:end\n' \

    assert exp == call(inp, 'foo', 'bar', 'baz')

    assert inp == call(exp)
    assert inp == call(exp, 'foo')
    assert inp == call(exp, 'foo', 'bar')
    assert inp == call(exp, 'foo', 'baz')
    assert inp == call(exp, 'bar')
    assert inp == call(exp, 'bar', 'baz')
    assert inp == call(exp, 'baz')

    inp = '# comm:begin foo && bar || baz\n' \
          '# foo bar baz\n' \
          '# comm:end\n' \

    exp = '# comm:begin foo && bar || baz\n' \
          'foo bar baz\n' \
          '# comm:end\n' \

    assert exp == call(inp, 'foo', 'bar', 'baz')
    assert exp == call(exp, 'foo', 'bar')
    assert exp == call(inp, 'baz')

    assert inp == call(exp)
    assert inp == call(exp, 'foo')
    assert inp == call(exp, 'bar')

    inp = '# comm:begin foo && bar || baz || blah\n' \
          '# foo bar baz blah\n' \
          '# comm:end\n' \

    exp = '# comm:begin foo && bar || baz || blah\n' \
          'foo bar baz blah\n' \
          '# comm:end\n' \

    assert exp == call(inp, 'foo', 'bar', 'baz', 'blah')
    assert exp == call(inp, 'foo', 'bar')
    assert exp == call(inp, 'baz', 'blah')
    assert exp == call(inp, 'blah')
    assert exp == call(inp, 'baz')

    assert inp == call(exp, 'foo')
    assert inp == call(exp, 'bar')

    inp = '# comm:begin foo && bar || (foo && baz)\n' \
          '# foo bar baz\n' \
          '# comm:end\n' \

    exp = '# comm:begin foo && bar || (foo && baz)\n' \
          'foo bar baz\n' \
          '# comm:end\n' \

    assert exp == call(inp, 'foo', 'bar')
    assert exp == call(inp, 'foo', 'baz')

    assert inp == call(exp, 'bar', 'baz')

def test_bool_parentheses(call):
    inp = '# comm:begin (foo)\n' \
          '# foo bar baz\n' \
          '# comm:end\n' \

    exp = '# comm:begin (foo)\n' \
          'foo bar baz\n' \
          '# comm:end\n' \

    assert exp == call(inp, 'foo')
    assert inp == call(inp, 'bar')

    inp = '# comm:begin baz && (foo || bar)\n' \
          '# foo bar baz\n' \
          '# comm:end\n' \

    exp = '# comm:begin baz && (foo || bar)\n' \
          'foo bar baz\n' \
          '# comm:end\n' \

    assert exp == call(inp, 'foo', 'bar', 'baz')
    assert exp == call(inp, 'bar', 'baz')
    assert exp == call(inp, 'foo', 'baz')

    assert inp == call(inp, 'foo')
    assert inp == call(inp, 'foo', 'bar')
    assert inp == call(inp, 'baz')

def test_bool_nested_parentheses(call):
    inp = '# comm:begin (bogus || blah) && (baz && (foo || bar))\n' \
          '# foo bar baz\n' \
          '# comm:end\n' \

    exp = '# comm:begin (bogus || blah) && (baz && (foo || bar))\n' \
          'foo bar baz\n' \
          '# comm:end\n' \

    assert exp == call(inp, 'blah', 'bogus', 'blah', 'baz', 'foo', 'bar')

    assert exp == call(inp, 'bogus', 'blah', 'baz', 'foo', 'bar')
    assert exp == call(inp, 'bogus', 'baz', 'foo')
    assert exp == call(inp, 'bogus', 'baz', 'bar')

    assert exp == call(inp, 'blah', 'blah', 'baz', 'foo', 'bar')
    assert exp == call(inp, 'blah', 'baz', 'foo')
    assert exp == call(inp, 'blah', 'baz', 'bar')

    assert inp == call(inp)
    assert inp == call(inp, 'bogus')
    assert inp == call(inp, 'blah')
    assert inp == call(inp, 'blah', 'bogus')

    assert inp == call(inp, 'blah', 'bar')
    assert inp == call(inp, 'blah', 'foo')
    assert inp == call(inp, 'blah', 'foo', 'bar')
    assert inp == call(inp, 'blah', 'baz')

    assert inp == call(inp, 'bogus', 'bar')
    assert inp == call(inp, 'bogus', 'foo')
    assert inp == call(inp, 'bogus', 'foo', 'bar')
    assert inp == call(inp, 'bogus', 'baz')


def test_uncomment_only_beginning_of_line(call):
    inp = '# comm:begin foo\n' \
          '# foo # some comment\n' \
          '# comm:end\n'

    exp = '# comm:begin foo\n' \
          'foo # some comment\n' \
          '# comm:end\n'

    assert exp == call(inp, 'foo')


def test_uncomment_single(call):
    inp = '# comm:begin foo\n' \
          '# # # foo\n' \
          '# comm:end\n'

    exp = '# comm:begin foo\n' \
          '# # foo\n' \
          '# comm:end\n'

    assert exp == call(inp, 'foo')


def test_indent(call):
    inp = '# comm:begin foo\n' \
          '    # foo\n' \
          '# comm:end\n'

    exp = '# comm:begin foo\n' \
          '    foo\n' \
          '# comm:end\n'

    assert exp == call(inp, 'foo')
    assert inp == call(exp, 'blah')


def test_force_uncomment(call):
    inp = 'blah blah\n' \
          '# blah blah\n' \
          '#blah blah\n' \
          '# comm:begin foo\n' \
          '    # foo\n' \
          '# comm:end\n' \
          'blah blah\n' \
          '# blah blah\n' \
          '#blah blah\n'

    exp = 'blah blah\n' \
          '# blah blah\n' \
          '#blah blah\n' \
          '# comm:begin foo\n' \
          '    foo\n' \
          '# comm:end\n' \
          'blah blah\n' \
          '# blah blah\n' \
          '#blah blah\n'

    opts = {'--force-uncomment': None}
    assert exp == call(inp, **opts)
    assert exp == call(inp, 'foo', **opts)
    assert exp == call(inp, 'bar', **opts)
    assert exp == call(inp, 'bar', 'baz', **opts)

    assert exp == call(exp, **opts)
    assert exp == call(exp, **opts)
    assert exp == call(exp, 'foo', **opts)
    assert exp == call(exp, 'bar', **opts)
    assert exp == call(exp, 'bar', 'baz', **opts)


def test_force_uncomment_nested(call):
    inp = '# comm:begin foo\n' \
          '# foo\n' \
          '    # comm:begin bar\n' \
          '    # bar\n' \
          '    # comm:end\n' \
          '# comm:end\n' \

    exp = '# comm:begin foo\n' \
          'foo\n' \
          '    # comm:begin bar\n' \
          '    bar\n' \
          '    # comm:end\n' \
          '# comm:end\n' \

    opts = {'--force-uncomment': None}
    assert exp == call(inp, **opts)
    assert exp == call(inp, 'foo', **opts)
    assert exp == call(inp, 'bar', **opts)
    assert exp == call(inp, 'bar', 'baz', **opts)

    assert exp == call(exp, **opts)
    assert exp == call(exp, **opts)
    assert exp == call(exp, 'foo', **opts)
    assert exp == call(exp, 'bar', **opts)
    assert exp == call(exp, 'bar', 'baz', **opts)
