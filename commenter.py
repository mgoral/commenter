# Copyright (C) 2017 Michał Góral
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import re
import argparse
import enum
import collections
import contextlib

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

class Actions(enum.Enum):
    comment = 1
    uncomment = 2
    pass_ = 3


class BlockStack:
    _stack_elem = collections.namedtuple("StackElement", ['prefix', 'action'])

    def __init__(self):
        self._stack = []

    def push(self, prefix, action):
        self._stack.append(self._stack_elem(prefix, action))

    def pop(self):
        return self._stack.pop()

    @property
    def action(self):
        return self._stack[-1].action

    @property
    def prefix(self):
        return self._stack[-1].prefix

    def __len__(self):
        return len(self._stack)


def _tokenize(s, defines):
    tokens = {}

    for d in defines:
        tokens[d] = 'True'

    tokens['&&'] = 'and'
    tokens['||'] = 'or'
    tokens['('] = '('
    tokens[')'] = ')'
    tokens['!'] = ' not'

    s = s.replace('(', ' ( ').replace(')', ' ) ').replace('!', ' ! ')
    return [tokens.get(elem, 'False') for elem in s.split()]

def find(lst, what, start=0):
    return [i for i,it in enumerate(lst) if it == what and i >= start]


def find_closing_parentheses(tokens, start=0):
    nested_open = 0
    for i, token in enumerate(tokens[start:]):
        if token == '(':
            nested_open += 1
        elif token == ')':
            if nested_open > 0:
                nested_open -= 1
            else:
                return i + start
    eprint("Missing closing parentheses")
    sys.exit(1)


def eval_tokenized(tokens):
    if not tokens:
        return False
    return eval(" ".join(tokens))


def should_comment(expr, defines):
    tokens = _tokenize(expr, defines)
    try:
        result = eval_tokenized(tokens)
    except SyntaxError as e:
        eprint("Syntax error: %s" % e)
        eprint("  in line: %s " % expr)
        sys.exit(1)
    if result is True:
        return Actions.uncomment
    elif result is False:
        return Actions.comment


def comment(line, prefix, action):
    if action == Actions.comment:
        if line.strip().startswith(prefix):
            return line
        return re.sub(r'(\S)', r'%s\1' % prefix, line, 1)
    elif action == Actions.uncomment:
        pattern = r'^(\s*)%s' % prefix
        return re.sub(pattern, r'\1', line)


def parse(lines, args):
    begin = re.compile(r'^\s*(?P<prefix>\S+\s*)comm:begin\s+(?P<pattern>[\w!|&() ]+)')
    end = re.compile(r'^\s*(?P<prefix>\S+\s*)comm:end')

    stack = BlockStack()
    stack.push(prefix=None, action=Actions.pass_)

    for line in lines:
        bm = begin.match(line)
        if bm is not None:
            if stack.action == Actions.comment:
                stack.push(bm.group('prefix'), Actions.comment)
            else:
                stack.push(bm.group('prefix'),
                           should_comment(bm.group('pattern'), args.defines))
            yield line
            continue

        em = end.match(line)
        if em is not None:
            stack.pop()
            yield line
            continue

        if args.force_uncomment is True:
            yield comment(line, stack.prefix, Actions.uncomment)
        elif stack.action != Actions.pass_:
            yield comment(line, stack.prefix, stack.action)
        else:
            yield line

    if len(stack) > 1:
        eprint("unterminated block")


@contextlib.contextmanager
def read_input(infile):
    ''' Reads input either from file or stdin.'''
    if infile is None:
        yield sys.stdin
    else:
        try:
            f = open(infile)
        except FileNotFoundError:
            eprint("File not found: %s" % infile)
            sys.exit(1)

        try:
            yield f
        finally:
            f.close()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', dest='infile',
                        help='input file. By default input is read from stdin')
    parser.add_argument('-D', dest='defines', action='append', default=[],
                        help='list of block names that should be uncommented')
    parser.add_argument('-u', '--force-uncomment', action='store_true',
                        help='skip checking selected defines and force '
                              'uncommenting  all sections')
    args = parser.parse_args()
    return args


def main():
    args = parse_args()

    with read_input(args.infile) as f:
        for line in parse(f, args):
            sys.stdout.write(line)
    return 0
