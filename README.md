# Commenter

Commenter is a filter which comments or uncomments blocks of code, depending on
set of defines. It works in a similar fashion to cpp (C Preprocessor).

It was inspired by [mir.qualia][qualia].

  [qualia]: https://github.com/darkfeline/mir.qualia

## Usage

Each block is specified by a set of words forming some kind of boolean
expression. Presence of these words is checked against defines passed to
commenter during its invocation (with `-D` flag; it can be set multiple times).

Example block looks like this:

    # comm:begin define
    block specific contents
    # comm:end

Example usage for such file would produce the following results:

    $ commenter < infile | tee outfile

    # comm:begin define
    # block specific contents
    # comm:end

    $ commenter -D define < outfile

    # comm:begin define
    block specific contents
    # comm:end

Commenter is a filter - it doesn't change contents of files by itself.
Redirect its output to achieve that.

Block header (`comm:begin`) can contain a boolean expressions. Any set of
logical and (`&&`), logical or (`||`), negation (`!`) and grouping with
parentheses is supported:

    # comm:begin foo || (bar && baz)
    contents
    # comm:end

Blocks can be nested. Outer blocks have precedence over the inner ones (i.e. if
outer block will be commented out, then even if inner's one header would
evaluate to true, the whole block would be left commented out):

    $ cat infile
        # comm:begin foo
        contents foo
        # comm:begin bar && baz
        contents bar baz
        # comm:end
        # comm:end

    $ commenter -D bar -D baz < infile
        # comm:begin foo
        # contents foo
        # comm:begin bar && baz
        # contents bar baz
        # comm:end
        # comm:end

    $ commenter -D foo < infile
        # comm:begin foo
        contents foo
        # comm:begin bar && baz
        # contents bar baz
        # comm:end
        # comm:end
